package LibraryMath;

import static java.lang.Math.*;

import java.util.Scanner;

public class ProjectileFlightDistance {

    public static void main(String[] args) {
        double g = 9.8;
        Scanner scan = new Scanner(System.in);
        System.out.println("Введинте угол возвышения ствола гаубицы в радианах: ");
        double a = scan.nextDouble();
        System.out.println("Введинте начальную скорость полёта снаряда: ");
        double v = scan.nextDouble();
        double distanceDegrees = ((pow(v, 2) * sin(2 * toDegrees(a))) / g);
        double distancesRadian = ((pow(v, 2) * sin(2 * a)) / g);
        System.out.println("Расстояние, где угол в градусах: " + abs(distanceDegrees));
        System.out.println("Расстояние, где угол в радианах: " + abs(distancesRadian));
    }
}

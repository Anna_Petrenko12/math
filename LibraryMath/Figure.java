import java.util.Scanner;

public class Figure {
    public static void main(String[] args) {

        float x = 2f;
        float y = 0.5f;

        boolean pointInRegion = pointIn(x, y);

        System.out.println("Point is in area: " + pointInRegion);
    }

    public static boolean pointIn(float x, float y) {

        boolean pointIn = false;

        if ((x >= 0) && (y >= 1.5 * x - 1) && (y <= x) || (x <= 0) && (y >= -1.5 * x - 1) && (y <= -x)) {
            pointIn = true;
        }

        return pointIn;
    }
}

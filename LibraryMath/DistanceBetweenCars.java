package LibraryMath;

import java.util.Scanner;

public class DistanceBetweenCars {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите скорость первого автомобиля: ");
        double v1 = scan.nextDouble();
        System.out.println("Введите скорость второго автомобиля: ");
        double v2 = scan.nextDouble();
        System.out.println("Введите растояние между автомобилями: ");
        double s = scan.nextDouble();
        System.out.println("Введите время: ");
        double t = scan.nextDouble();
        System.out.println("прошел первый автомобиль:" + distanceFirstCar(v1, t));
        System.out.println("прошел второй автомобиль:" + distanceSecondCar(v2, t));
        double distanceBetweenCars = distanceFirstCar(v1, t) + distanceSecondCar(v2, t) + s;
        System.out.println("Расстояние между автомобилями:" + distanceBetweenCars);
    }

    public static double distanceFirstCar(double v1, double t) {
        return v1 * t;
    }

    public static double distanceSecondCar(double v2, double t) {
        return v2 * t;
    }
}

package Figures;

public class Rhombus {
    public static void main(String[] args) {
        int num = 10;
        for (int i = 1; i < num; ++i) { //Сделать ромб в 2 раза меньше num/2

            for (int j = num; j > i; --j)
                System.out.print(" ");

            for (int j = 1; j < 2 * i; ++j)
                System.out.print("*");

            System.out.println();
        }
        for (int i = num; i >= 1; --i) { //Сделать ромб в 2 раза меньше num/2

            for (int j = num; j > i; --j)
                System.out.print(" ");

            for (int j = 1; j < i * 2; ++j)
                System.out.print("*");

            System.out.println();
        }
    }
}
